# Multi-Party Computation (MPC) Wallet Service
Clone & updated from: [https://github.com/sunnyMunna/mpcwallet/tree/main](https://github.com/sunnyMunna/mpcwallet/tree/main)

## API Endpoints
This MPC wallet service exposes the following API endpoints:

### POST /createWallet 
Generates a new Ethereum wallet and returns its address, secret key, and mnemonic phrase.
#### response
```json
{
  "address": "0xEc...",
  "secretKey": "....",
  "mnemonic": "a seed phrase..."
}
```

### POST /createShares 
Create shares from a secret key using MPC principles and returns the shares.
#### body
```json
{ 
  "secretKey": "...", 
  "numShares": "3",
  "threshold": "2" 
}
```
#### response
```json
{
  "shares": [
    {
      "x": "1",
      "y": "..."
    },
    {
      "x": "2",
      "y": "..."
    },
    {
      "x": "3",
      "y": "..."
    }
  ],
  "threshold": "2",
  "prime": "..."
}
```

### POST /recoverSecret 
Reconstructs the secret key from a subset of shares using MPC techniques.
#### body
```json
{
  "shares": [
    {
      "x": "1",
      "y": "..."
    },
    {
      "x": "2",
      "y": "..."
    }
  ],
  "threshold": "2",
  "prime": "..."
}
```
#### response
```json
{
  "secretKey": "..."
}
```
