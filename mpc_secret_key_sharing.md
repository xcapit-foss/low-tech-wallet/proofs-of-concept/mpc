# **Using MPC for Securing User Wallet Secret Keys**

### Introduction to MPC

Multiparty Computation (MPC) is a subfield of cryptography that enables multiple parties to jointly perform computations on their data without revealing the underlying private information. A commonly cited example is the calculation of the average salary in a group without any participant disclosing their individual salary. Using MPC, each participant encrypts and divides their data. The encrypted computations yield the desired result, such as the average salary, while maintaining data privacy.

### Shamir's Secret Sharing Scheme

Within the MPC domain, Shamir’s Secret Sharing Scheme (SSSS), introduced by Adi Shamir in 1979, is a foundational cryptographic approach. It allows sensitive data, such as cryptographic keys, to be divided into multiple parts or "shares." These shares are distributed among participants, ensuring that only a predefined minimum number (threshold) of shares is required to reconstruct the original secret. For example, if the threshold is set to 2, at least two participants are necessary to reconstruct the secret.

### Practical Application of SSSS in Blockchain Wallets

In blockchain, SSSS is used to enhance the security of wallet secret keys by splitting them into multiple shares. Here’s how it works:

1. **Polynomial Creation:**
   A secret key, represented as a number, is embedded as the constant term of a polynomial. The degree of the polynomial is , where  is the threshold number of shares required to reconstruct the secret.

   For example, with a threshold of 2, a linear polynomial () is generated, where  is the secret key and  is a randomly chosen coefficient.

2. **Share Generation:**
   The polynomial is evaluated at random points (e.g., ) to produce pairs of values, known as shares.

3. **Reconstruction of the Secret:**
   To reconstruct the secret, at least  shares are required. Using Lagrange interpolation, the original polynomial can be recreated, and the secret key () can be extracted.

### Modular Arithmetic in SSSS

To ensure computational efficiency and prevent excessively large numbers, SSSS employs modular arithmetic. All operations (addition, multiplication, etc.) are performed modulo a prime number. This approach keeps values within manageable bounds and ensures the mathematical correctness of the scheme.

### Implementation Example

An implementation of SSSS for a blockchain wallet might involve the following steps:

1. **Wallet Creation:**
   Generate a wallet and retrieve its secret key.

2. **Share Distribution:**
   Split the secret key into shares using SSSS. Specify the number of shares and the threshold for reconstruction.

3. **Share Storage:**
   Distribute shares securely. For instance, one share could be stored on the user’s device, another on a UNICEF server, and a third on Xcapit’s server.

4. **Secret Reconstruction:**
   When needed, at least  shares are retrieved and used to reconstruct the secret key via Lagrange interpolation.

### API Requests for Practical Testing

#### Creating Shares

The following API request is used to generate shares from a secret key:

```json
{
  "secretKey": "f4f1e6d05f24a1d8284d43f898b174908ce474a46e0dacfdbd0811e9a44c0240",
  "numShares": 10,
  "threshold": 2
}
```

This request includes the secret key, the number of shares to generate, and the minimum number of shares required for reconstruction.

#### Reconstructing the Key

To reconstruct the secret key from the shares, the following API request is used:

```json
{
  "shares": [
    {
      "x": "7",
      "y": "a4d7b87cd0e7d85e7162524683bcff0c21a75e9f4a10467e1ed7e7e55d58058e20d409b09e848a940ec5ff804141a2583dc4dadba03b8e0f092275ddefa2f67d"
    },
    {
      "x": "8",
      "y": "2e85d78cd32caaf5f0f2b894b70d769026283be9a3c1d04f39c00c5abca0abd7e0b4de5bbf6095fbac0bc81dc5efd760573e396bb0544d9bd78c95fddbee1597"
    }
  ],
  "threshold": 2,
  "prime": "10402379994141958186914094494486799320959919790586131106656425679218803484819781050966922567991144747150561511379184445351515546075274336700067574867677723"
}
```

This request provides the necessary shares, the threshold, and the prime number used for modular arithmetic.

### Benefits and Security Considerations

The use of SSSS offers:

- **Redundancy:** Even if several shares are lost, the secret can still be reconstructed using the minimum threshold of shares.
- **Privacy and Security:** No single entity has full access to the secret key, reducing the risk of compromise.
- **Off-Chain Processing:** Unlike multisig solutions, SSSS operates off-chain, making it faster, cheaper, and more private.

However, during the secret reconstruction, the full private key exists temporarily in memory, presenting a potential vulnerability. Advanced implementations can eliminate this risk by enabling operations directly on the shares without reconstructing the key.

### Conclusion

MPC, and specifically Shamir’s Secret Sharing Scheme, provides a robust solution for securely managing blockchain wallet secret keys. By distributing shares among trusted parties and requiring a threshold for reconstruction, this approach enhances security while maintaining operational flexibility. Its off-chain nature ensures cost-effectiveness and privacy, making it a valuable tool for applications in blockchain and beyond.

